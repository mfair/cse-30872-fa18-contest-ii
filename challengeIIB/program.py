#!/usr/bin/env python3
# inspired by squirrel.py and the least kakamora problem
# Marybeth Fair
# Challenge IIB

# Imports
import sys
import math

# Functions
def read_grid(h, w):
  # reads the grid from stdin
  # grid = [[math.inf for _ in range(width)]]
  grid = []
  for _ in range(height):
    # grid.append([math.inf] + list(map(int, sys.stdin.readline().split())))
    grid.append(list(map(int, sys.stdin.readline().split())))

  return grid

def least_kakamora(grid, h, w, s):
  # fills the memoization table for the last kakamora to get to any spot from the upper left corner
  table = [[math.inf for _ in range(w)] for _ in range(h)]
  table[s][0] = grid[s][0]

  for col in range(w):
    for row in range(h):
      row_up = row + 1
      if row_up >= h:
        row_up = 0
      row_down = row - 1
      if row_down < 0:
        row_down = h - 1
      if col-1 < 0:
        continue
      table[row][col] = grid[row][col] + min(
        table[row_down][col-1],
        table[row][col-1],
        table[row_up][col-1]
      )
  return table

def reconstruct_path(height, width, table, grid, k, h, w, min_start):
  # go back and reconstruct the path using the maximmum
  x = h
  y = w
  path = []
  while y > 0:
    path.append(x+1)
    k -= grid[x][y]
    if x < height - 1:
      if k == table[x+1][y-1]:
        x = x +1
        y=y-1
        continue
    else:
      if k == table[0][y-1]:
        x = 0
        y = y-1
        continue
    if x > 0:
      if k == table[x-1][y-1]:
        x = x - 1
        y=y-1
        continue
    else:
      if k == table[height-1][y-1]:
        x = height-1
        y = y-1
        continue
    y = y - 1

  path.append(x+1)
  return path


# Main
if __name__ == "__main__":
  while True:
    try:
      # read input until the end
      params = sys.stdin.readline().split()
    except ValueError:
      break
    try:
      height = int(params[0])
      width = int(params[1])
    except IndexError:
      break
    grid = read_grid(height, width)
    min_val = math.inf
    min_table = []
    min_spot = 0
    min_start = 0
    for i in range(height):
      table = least_kakamora(grid, height, width, i)
      for j, line in enumerate(table):
        if line[width-1] < min_val:
          min_val = line[width-1]
          min_table= table
          min_spot = j
          min_start = i
    print(min_table[min_spot][width-1])
    path = reconstruct_path(height, width, min_table, grid, min_val, min_spot, width-1, min_start)
    path = reversed(path)
    print(*path, sep=" ")
