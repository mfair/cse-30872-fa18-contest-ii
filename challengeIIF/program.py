#!/usr/bin/env python3

import sys
import math

def find_min_miles(num):
	
	cals = [ int(x) for x in sys.stdin.readline().rstrip().split() ]
	cals = sorted(cals,reverse=True)
	miles = 0
	for index,cal in enumerate(cals):
		miles += math.pow(2,index) * cal
	return miles

if __name__ =='__main__':
	numTacos = 0
	calories = []
	for line in sys.stdin:
		numTacos = int(line.rstrip())
		minMiles = find_min_miles(numTacos)
		print(int(minMiles))
