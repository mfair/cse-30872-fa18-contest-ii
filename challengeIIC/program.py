#!/usr/bin/env python3

import sys

def find_long_perm(a,b):
	perm = []
	aDict = {}
	bDict = {}
	for char in a:
		if char in aDict:
			aDict[char] += 1
		else:	
			aDict[char] = 1
	for char in b:
		if char in bDict:
			bDict[char] += 1
		else:	
			bDict[char] = 1
	for char in aDict:
		if char in bDict:
			for num in range(min(aDict[char],bDict[char])):
				perm.append(char)
				
			
	perm = sorted(perm)
	print(''.join(perm))

if __name__ == '__main__':
	
	for line in sys.stdin:
		find_long_perm(line.rstrip(),sys.stdin.readline().rstrip())
	
