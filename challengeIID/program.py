#!/usr/bin/env python3

import sys

def is_seq(wset):
    for i,w in enumerate(wset[:-1]):
        j = 0
        k = 0
        switch = False
        word1 = wset[i]
        word2 = wset[i + 1]
        while j < len(w) and k < len(wset[i + 1]):
            if word1[j] == word2[k]:
                j += 1
                k += 1
                continue
            elif len(word1) - len(word2) == -1 and word1[j] == word2[k + 1]:
                if switch:
                    return False
                k += 1
                switch = True
                continue
            elif len(word1) - len(word2) == 1 and word1[j+1] == word2[k]:
                if switch:
                    return False
                j += 1
                switch = True
                continue
            elif len(word1) == len(word2):
                if switch:
                    return False
                switch = True
                j += 1
                k += 1
            else:
                return False
    return True

def find_morphs(words, i, wset):
    if i == len(words):
        if is_seq(wset):
            return wset
        else:
            return None
    s_in = []
    s_in.extend(wset)
    s_in.append(words[i])
    not_include = find_morphs(words, i + 1, s_in)  
    s_nin = []
    s_nin.extend(wset)
    include = find_morphs(words, i + 1, s_nin)
    if not include and not not_include:
        return None
    if include and not not_include:
        return include
    if not include and not_include:
        return not_include
    if include and not_include:
        if len(include) >= len(not_include):
            return include
        else:
            return not_include

if __name__ == "__main__":
    words = []
    for line in sys.stdin:
        words.append(line.rstrip())
    words = sorted(words)
    seq = find_morphs(words, 0, [])
    print(len(seq))
    for s in seq:
        print(s)

    '''longest = 0
    for i,w in enumerate(words[:-1]):
        print()
        print(w)
        word1 = w
        word2 = words[i + 1]
        w1i = i
        w2i = i + 1
        while word2 != words[-1]:
            print("w1:", word1)
            print("w2:", word2)
            j = 0
            k = 0
            switch = False
            while j < len(word1) and k < len(word2):
                print("j:", j)
                print("k:", k)
                if word1[j] == word2[k]:
                    j += 1
                    k += 1
                    continue
                elif len(word1) - len(word2) == -1 and word1[j] == word2[k+1]:
                    if switch:
                        word2 = words[w1i + 2]
                        w2i += 1
                        break
                    k += 1
                    switch = True
                    continue
                elif len(word1) - len(word2) == 1 and word1[j+1] == word2[k]:
                    if switch:
                        word2 = words[w2i + 1]
                        w2i += 1
                        break 
                    j += 1
                    switch = True
                    continue
                elif len(word1) == len(word2):
                    if switch:
                        word2 = words[w2i + 1]
                        w2i += 1
                        break
                    switch = True
                    j += 1
                    k += 1
                else:
                    #print("here")
                    word2 = words[w2i + 1]
                    w2i += 1
                    break 
            if j == len(word1) and k == len(word2): #eh watch
                if w2i + 1== len(words):
                    break
                word1 = word2
                word2 = words[w2i + 1]
                w1i += 1
                w2i += 1
            else:
                if w2i + 1 == len(words):
                    break
                print("hey")
                word2 = words[w2i + 1]
                w2i += 1'''
                
# vim: set sts=4 sw=4 ts=4 expandtab ft=python:
